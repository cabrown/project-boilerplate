module.exports = function (grunt) {

  'use strict';

  /**
   *  Load Grunt tasks (from devDependencies in package.json)
   */

  require('load-grunt-tasks')(grunt, {
    scope: 'devDependencies'
  });

  /**
   *  Measure build time
   */

  require('time-grunt')(grunt);

  /**
   *  Config
   */

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json')
  });

  /**
   *  Load tasks
   */

  grunt.loadTasks('grunt/tasks');

  /**
   *  Register tasks
   */

  grunt.registerTask('default', ['cssbuild', 'jsbuild']);
  grunt.registerTask('test', ['jshint', 'mocha']);
  grunt.registerTask('cssbuild', ['clean:css', 'sprite', 'sass:dist', 'autoprefixer']);
  grunt.registerTask('jsbuild', ['jshint', 'clean:js', 'browserify:test', 'mocha', 'browserify:dist', 'uglify']);

};