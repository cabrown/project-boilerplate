'use strict';

var expect = chai.expect;

define([
    'jquery',
    'js/components/example.requirejs'
  ], function ($, example) {

  describe('$.fn.example()', function () {
    describe('testFunc()', function () {
      it('should return  a \'testFunc\' string', function () {
        expect(example().testFunc()).to.equal('testFunc');
      });
    });
  });

});