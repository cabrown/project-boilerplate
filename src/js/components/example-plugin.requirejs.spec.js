'use strict';

var expect = chai.expect;

define([
    'jquery',
    'js/components/example-plugin.requirejs'
  ], function ($, examplePlugin) {

  describe('$.fn.examplePlugin()', function () {
    it('the global options.foo variable to equal \'bar\'', function () {
      expect($.fn.examplePlugin.options.foo).to.equal('bar');
    });
    it('to return the selector name (.foo)', function () {
      expect($('.foo').examplePlugin().selector).to.equal('.foo');
    });
  });

});