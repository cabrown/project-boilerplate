define([
    'jquery'
  ], function ($) {

  'use strict';

  $.fn.example = function (options) {

      // Defaults
      var defaults = {
      };

      options = $.extend(defaults, options);

      function testFunc () {
        return 'testFunc';
      }

      return {
        testFunc: testFunc
      };

  };

  return $.fn.example;

});