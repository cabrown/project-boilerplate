define([
    'jquery'
  ], function ($) {

  'use strict';

  $.fn.examplePlugin = function (options) {

    options = $.extend({}, $.fn.examplePlugin.options, options);

    return this.each(function () {
    });

  };

  $.fn.examplePlugin.options = {
    foo: 'bar'
  };

  return $.fn.examplePlugin;

});