'use strict';

var $ = require('../../vendor/jquery-1.11.1/index');

$.fn.examplePlugin = function (options) {

  options = $.extend({}, $.fn.examplePlugin.options, options);

  return this.each(function () {
      $('<div />')
        .attr('class', 'bar')
        .html(options.foo)
        .appendTo($(this));
  });

};

$.fn.examplePlugin.options = {
  foo: 'bar'
};

if (module && module.exports) {
  module.exports = $.fn.examplePlugin;
} else {
  global.$.fn.examplePlugin = $.fn.examplePlugin;
}