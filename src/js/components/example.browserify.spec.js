'use strict';

var $ = require('../../vendor/jquery-1.11.1/index'),
  example = require('./example.browserify'),
  expect = chai.expect;

describe('$.fn.example()', function () {
  describe('testFunc()', function () {
    it('should return  a \'testFunc\' string', function () {
      expect(example().testFunc()).to.equal('testFunc');
    });
  });
});