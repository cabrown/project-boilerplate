'use strict';

var $ = require('../../vendor/jquery-1.11.1/index'),
  examplePlugin = require('./example-plugin.browserify'),
  expect = chai.expect;

describe('$.fn.examplePlugin()', function () {
  it('the global options.foo variable to equal \'bar\'', function () {
    expect($.fn.examplePlugin.options.foo).to.equal('bar');
  });
  it('to return the selector name (.foo)', function () {
    expect($('.foo').examplePlugin().selector).to.equal('.foo');
  });
});