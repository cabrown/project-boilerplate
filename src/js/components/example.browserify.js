'use strict';

var $ = require('../../vendor/jquery-1.11.1/index');

$.fn.example = function (options) {

    // Defaults
  var defaults = {
  };

  options = $.extend(defaults, options);

  function testFunc () {
    return 'testFunc';
  }

  return {
    testFunc: testFunc
  };

};

if (module && module.exports) {
  module.exports = $.fn.example;
} else {
  global.$.fn.example = $.fn.example;
}