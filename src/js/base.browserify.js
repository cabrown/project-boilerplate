/**
 *  Base
 */

'use strict';

var $ = require('../vendor/jquery-1.11.1/index'),
  example = require('./components/example.browserify'),
  examplePlugin = require('./components/example-plugin.browserify');

$(function () {
  example().testFunc();
  $('.foo').examplePlugin();
});