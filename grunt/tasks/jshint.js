/**
 *  JS Hint
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('jshint', {
    options: {
      jshintrc: '.jshintrc'
    },
    all: ['Gruntfile.js', 'src/js/**/*.js']
  });

};