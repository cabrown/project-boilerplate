/**
 *  Autoprefixer
 */ 

 module.exports = function (grunt) {

  'use strict';

  grunt.config('autoprefixer', {
    options: {
      browsers: ['last 4 versions', 'ie 8', 'ie 9'],
      map: true
    },
    multiple_files: {
      expand: true,
      flatten: true,
      cwd: '<%= dirs.dist.css %>',
      src: ['**/*.css'],
      dest: '<%= dirs.dist.css %>'
    }
  });

 };