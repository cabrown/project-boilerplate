/**
 *  Browserify
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('browserify', {
    test: {
      src: ['./<%= dirs.src.js %>/**/*.browserify.spec.js'],
      dest: '<%= dirs.dist.js %>/specs.js'
    },
    dist: {
      files: {
        './<%= dirs.dist.js %>/base.js': ['./<%= dirs.src.js %>/base.browserify.js'],
      }
    }
  });

};