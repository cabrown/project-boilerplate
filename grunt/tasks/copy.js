/**
 *  Copy
 *
 *  @func     bower
 *  @returns  Copy Bower component *.css to _*.scss partials to enable @import within Sass
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('copy', {
    bower: {
      files: [{
        expand: true,
        cwd: '<%= dirs.src.vendor %>',
        src: ['**/*.css', '**/*.min.css'],
        dest: '<%= dirs.src.vendor %>',
        rename: function (dest, src) {
          var index = src.lastIndexOf('/');
          return dest + '/' + src.substr(0, index + 1) + '_' + src.substr(index + 1);
        },
        filter: 'isFile',
        ext: '.scss'
      }]
    }
  });

  grunt.registerTask('copy-bower', ['copy:bower']);

};