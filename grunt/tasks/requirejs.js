/**
 *  Require JS
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('requirejs', {
    compile: {
      options: {
        baseUrl: './<%= dirs.src.js %>',
        mainConfigFile: './<%= dirs.src.js %>/main-config.requirejs.js',
        paths: {
          'vendor': 'empty'
        },
        modules: [
          {
            name: 'base.requirejs'
          }
        ],
        dir: './<%= dirs.dist.js %>',
        optimize: 'uglify'
      }
    }
  });

};