/**
 *  Clean
 */ 

 module.exports = function (grunt) {

  'use strict';

  grunt.config('clean', {
    base: ['<%= dirs.dist.base %>'],
    css: ['<%= dirs.dist.css %>'],
    js: ['<%= dirs.dist.js %>']
  });

 };