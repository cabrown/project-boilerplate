/**
 *  Directories
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('dirs', {
    src: {
      base: 'src',
      font: 'src/font',
      img: 'src/img',
      js: 'src/js',
      scss: 'src/scss',
      vendor: 'src/vendor'
    },
    dist: {
      base: 'dist',
      css: 'dist/css',
      img: 'dist/img',
      js: 'dist/js'
    }
  });

};