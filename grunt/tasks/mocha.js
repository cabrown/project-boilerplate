/**
 *  Mocha
 */ 

module.exports = function (grunt) {

  'use strict';

  var path = require('path');

  grunt.config('mocha', {
    options: {
      run: true,
    },
    test: {
      src: [path.resolve() + '/<%= dirs.src.js %>/test/runner.browserify.html']
    },
  });

}