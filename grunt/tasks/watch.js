/**
 *  Watch
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('watch', {
    sass: {
      files: ['<%= dirs.src.scss %>/**/*.{scss,sass}'],
      tasks: ['sass:dist']
    }
  });

};