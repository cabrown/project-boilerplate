/**
 *  Sass
 */ 

module.exports = function (grunt) {

  'use strict';

  grunt.config('sass', {
    options: {
      sourceComments: 'map',
      sourceMap: true,
      outputStyle: 'compressed'
    },
    dist: {
      files: [{
        expand: true,
        cwd: '<%= dirs.src.scss %>',
        src: ['**/*.scss'],
        dest: '<%= dirs.dist.css %>',
        ext: '.css'
      }]
    }
  });

};