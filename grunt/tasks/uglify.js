/**
 *  Uglify
 */

module.exports = function (grunt) {
  
  grunt.config('uglify', {
    build: {
      src: './<%= dirs.dist.js %>/base.js',
      dest: './<%= dirs.dist.js %>/base.min.js'
    }
  });

};