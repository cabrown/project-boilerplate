/**
 *  Spritesmith
 */

module.exports = function (grunt) {

  'use strict';

  grunt.config('sprite', {
    all: {
      src: '<%= dirs.src.img %>/sprites/*.png',
      destImg: '<%= dirs.dist.img %>/sprites/spritesheet.png',
      destCSS: '<%= dirs.src.scss %>/helpers/_sprites.scss',
      imgPath: '/img/sprites/spritesheet.png',
    }
  });

};